<?php

require_once 'bootstrap.php';

header('Content-type: text/html; charset=UTF-8');
/* Silex */
$app = new Silex\Application();
$app['debug'] = true; // (getenv('APPLICATION_ENV') == 'development');
$app['uri'] = $_SERVER['REQUEST_URI'];

$app['dm'] = include('doctrine.php');

/* Providers */
$app->register(new Silex\Provider\TwigServiceProvider(), ['twig.path' => __DIR__.'/view']);
$app->register(new \Silex\Provider\LocaleServiceProvider());
$app->register(new Silex\Provider\TranslationServiceProvider(), ['locale_fallbacks' => ['pt_BR']]);
$app->register(new Silex\Provider\SessionServiceProvider());
$app->register(new Silex\Provider\FormServiceProvider());
$app->register(new Caf\Slugify\Bridge\Silex\SlugifyServiceProvider());
$app->register(new Silex\Provider\ValidatorServiceProvider());

$app->mount('/', new \Application\Controller\AccountController());
$app->mount('/', new \Application\Controller\HomeController());

$app->run();