<?php

namespace Application\Controller;

use Application\Document\User;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

class HomeController extends AbstractController
{

    /**
     * @param \Silex\ControllerCollection $controllers
     * @return \Silex\ControllerCollection
     */
    protected function mount(ControllerCollection $controllers)
    {
        $this->app->match('/', [$this, 'indexAction']);
        return $controllers;
    }

    public function indexAction(Application $app, Request $request)
    {
        return $this->render('index/index.twig');
    }
}