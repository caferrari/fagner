<?php

namespace Application\Controller;

use Application\Document\User;
use Silex\Application;
use Silex\ControllerCollection;
use Symfony\Component\HttpFoundation\Request;

class AccountController extends AbstractController
{

    /**
     * @param \Silex\ControllerCollection $controllers
     * @return \Silex\ControllerCollection
     */
    protected function mount(ControllerCollection $controllers)
    {
        $this->app->match('/register', [$this, 'registerAction']);
        $this->app->match('/login', [$this, 'loginAction']);
        return $controllers;
    }

    public function loginAction(Application $app, Request $request)
    {
        $form = $this->getForm('login');
        if ($form->hasPostData($request)) {
            if ($data = $form->getData()) {
                $repo = $this->getRepository('user');
                if ($user = $repo->authenticate($data)) {
                    $app['session']->set('user', $user);
                    $this->setFlashMessage('success', 'Você logou com sucesso!');
                    $app->redirect('/');
                }
                $app['session']->remove('uesr');
                $this->setFlashMessage('error', 'e-mail ou senha incorretos');
            }
        }

        return $this->render('account/login.twig', [
            'form' => $form->createView()
        ]);
    }

    public function registerAction(Application $app, Request $request)
    {
        $form = $this->getForm('register', new User);

        if ($form->hasPostData($request)) {
            if ($data = $form->getData()) {
                $repo = $this->getRepository('user');
                $repo->insert($data);
            }
        }

        return $this->render('account/register.twig', [
            'form' => $form->createView()
        ]);
    }
}