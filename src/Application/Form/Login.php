<?php

namespace Application\Form;

use Application\Document\User;
use Symfony\Component\Validator\Constraints as Assert;

class Login extends AbstractForm
{

    public function buildForm($factory, $entity = null)
    {
        return $factory->createBuilder('form')
            ->add('email', 'email')
            ->add('password', 'password')
            ->add('login', 'submit')
            ->getForm();
    }

}